import React, { Component } from 'react'
import './Notification.css'

export class Notification extends Component {
  render() {
    return (
      <div className="notification">
        <div className="wrapper">
          <div className="message">{/*This is the notification box element*/}
            <div className="messageLabel">
              {this.props.message}
            </div>
            <div className="description">
            <div className="date">{/*This is the date element*/}
              <div className="dateLabel">{/*This is the date tho be displayed element*/}
                {this.props.date}
              </div>
            </div>
            <div className="venu">
              {this.props.venue}
              </div>
            <div className="time">
              {this.props.time}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Notification

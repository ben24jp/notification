import React from 'react';
import Notification from './Components/Notification/Notification.jsx'
function App() {
  return (
    <div className="App"> 
      <Notification message='FAB LAB Meeting' date='06/02/2020' time='Time: 04:30 PM' venue='Main Seminar hall'/>
    </div>
  );
}

export default App;
